"""
This is an example data loader for pytorch which uses the datatool API to load the datatool output JSON and read samples.
The example task -- Provide description of example task

Input:
The input is a datatool output directory containing the dataset.json and samples directory

Assumptions are:
    - Assumption 1
    - Assumption 2
    - Assumption 3
"""

from torch.utils.data import Dataset
from torchvision.transforms import transforms
from PIL import Image
import os
import sys
import inspect

cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from datatool_api.config.APIConfig import DTAPIConfig
from datatool_api.models.DTDataset import DTDataset
from custom_dataset_model import DTDatasetCustom


class NVDataset(Dataset):
    def __init__(self, data_dir: str, operating_mode: str = 'memory', **kwargs):
        """
        Instantiate the dataset instance

        :param data_dir: Directory containing the datatool output which contains the "sample_files" directory and
        "dataset.json" file
        :param operating_mode: Operating mode for the datatool api to handle the dataset, based on the data size,
        user can choose [memory, disk or ramdisk]
        :param kwargs: Additional keyword arguments
        """
        Dataset.__init__(self)
        self.data_dir = data_dir
        self.operating_mode = operating_mode
        self.kwargs = kwargs

        # No need to validate when loading back the API output
        DTAPIConfig.disable_validation()
        # Load the dataset.json using the datatool API
        self.dataset = DTDataset(name='input_dataset',
                                 operatingMode=self.operating_mode).load_from_json(os.path.join(self.data_dir,
                                                                                                'dataset.json'),
                                                                                   element_list=['imageSamples'])

        # Example Transform to resize and normalize the images: Modify it to write a more complex transform
        self.transform = transforms.Compose([transforms.Resize(self.kwargs['model_input_size']),
                                             transforms.ToTensor(),
                                             transforms.Normalize(mean=self.kwargs['normalization']['mean'],
                                                                  std=self.kwargs['normalization']['std'])])

        # Add any extra needed variables here
        # TODO - Implemented by the datatool creator

    def __len__(self):
        # TODO - Implemented by the datatool creator
        pass

    def __getitem__(self, index):
        # TODO - Implemented by the datatool creator
        pass


def main():
    params = {
        'image_type': 'RGB',  # Image type that the model is going to take as input, in this case it is
        # 3 channel RGB
        'model_input_size': (200, 200),  # Width x Height of input tensor for the model
        'min_landmark_count': 17,  # Min no of 2d landmarks which must be present for a subject to include it
        # in loading candidate list
        'normalization': {  # Normalization parameters for image
            'mean': [154.78, 118.57, 101.74],
            'std': [53.80, 48.19, 46.15]
        }
    }

    # TODO: Set by the user
    data_dir = '<Input directory containing the samples and dataset.json>'

    # Create dataset instance
    dataset = NVDataset(data_dir=data_dir, operating_mode='memory', **params)
    for i in range(len(dataset)):
        print(dataset.__getitem__(i))


if __name__ == main():
    main()
