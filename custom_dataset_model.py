from __future__ import annotations
from datatool_api.models.base.BaseDTDataset import BaseDTDataset
from datatool_api.data_validation.containers.StorageValDict import StorageValDict
from datatool_api.data_validation.containers.ValDict import ValDict
from custom_base_types import *
from typing import Tuple
import pandas


class DTDatasetCustom(BaseDTDataset):
    """
    Implement your custom dataset model using this interface if working with a custom dataset model.
    Remove NotImplementedError from below if and only if "DTDatasetCustom" this class represents the dataset model i.e.
    at least one attribute is added to this class.
    """
    raise NotImplementedError

    def __init__(self, name: str, operatingMode: str):
        super().__init__(name, operatingMode)

    def __del__(self):
        super().__del__()

    """
    Needs to be implemented by the datatool creator
    """
    def to_pandas_frame(self, keep_original: bool = False, column_subset: List = None) -> List[Tuple[str,
                                                                                                     pandas.DataFrame]]:
        """
        Pandas dataframe generator for dataset

        :param keep_original: If original dataset needs to be kept in memory, if False, the original dataset object
        can be modified, by popping samples from it.
        :param column_subset: If only a subset of all columns are needed in the dataframe
        :return: List of (dataframe_name, pandas.Dataframe)
        """
        raise NotImplementedError

    def to_pandas_frame_for_report(self) -> List[Tuple[str, pandas.DataFrame, List[str]]]:
        """
        Get the pandas dataframe which is used for report generation. This method changes the column names to the
        expected names for columns in the report and only exports the columns which are required in the report.

        In addition it also export a column name list along with each dataframe holding the columns names which should
        be used to generate the interaction plots in the report.

        :return: List of (dataframe_name, pandas.Dataframe, List[columns_needed_for_interaction_plots])
        """
        raise NotImplementedError
