# {NAME} Data Tool
{DATATOOL DESCRIPTION}  

This data tool provides the annotations for:      
* {PLACEHOLDER 1}  
* {PLACEHOLDER 2}  
* {PLACEHOLDER 3}  
.  
.  
* {PLACEHOLDER N}


## Table of Contents
This document covers the following topics:
* [Overview](#overview)
* [Available Versions and Tags](#available-versions-and-tags)
* [How to Download Source Data](#how-to-download-the-source-dataset)
* [Example Samples and Annotations](#example-samples-and-annotations)
* [Installation](#installation)
* [Dataset Access](#dataset-access)
* [Usage](#usage)
  * [Run Datatool](#run-datatool)
  * [Output](#output)
  * [Exploratory Data Analysis (EDA) Report](#exploratory-data-analysis-(EDA)-report)
  * [Plot Annotations](#plot-annotations)
  * [Data Loader Examples](#data-loader-examples)
* [Maintenance](#maintenance)


## Overview
**1. Documentation**: Overall documentation including the example samples and statistics.
- [X] Documentation [README.md](/README.md) with example samples and annotations.
- [X] [Exploratory Data Analysis (EDA) Report](/eda_report) containing the annotation statistics, trends and interactions. 

**2. Main Interface**: Process the source input data and create the final standardized dataset.
- [X] Script [datatool.py](/datatool.py) to process source input data and generate standardized output.
- [X] Script [datatool_patch.py](/datatool_patch.py) to apply any available post-processing (cleaning, geometric transformations etc.) on the generated datatool output.
- [X] Dependencies [Datatool API Requirements](/datatool_api/deps/requirements.txt), [Common Requirements](/common/deps/requirements.txt) and [Datatool Specific Requirement](/deps/requirements.txt) contains the list of python modules needed by the datatool.

**3. Exploratory Data Analysis**: Create / Re-create an Exploratory Data Analysis report on datatool output for getting statistical insights.
- [X] Script [create_EDA_report.sh](/create_EDA_report.sh) to download and run the EDA report tool on datatool output and generate the EDA report. 

**4. Plot Annotations on Datatool Output**: Draw samples at random from datatool output and [draw annotations](/plot_annotations). This is useful in validation and verification of the annotations.
- [X] Script [plot_annotations.py](/plot_annotations/plot_annotations.py) to plot annotations on randomly drawn samples from datatool output.
- [X] Dependencies [requirements.txt](/plot_annotations/requirements.txt) required list of python modules needed by the script.

**5. Data Loader Example**: Example of how to easily load the datatool output for model training/validation using the datatool API.
- [X] Script [example_dataloader_pytorch.py](/example_dataloaders/example_dataloader_pytorch.py) provides a data loader example for pytorch using the datatool API.


## Available Versions and Tags
This datatool supports the following versions and tags:

| Version   | Tag   | Tag Type                         | #Samples       | #Labels per Sample       | #Annotations                          | Description   |
|-----------|-------|----------------------------------|----------------|--------------------------|---------------------------------------|---------------|
| {VERSION} | {TAG} | {TAG TYPE (Physical or Logical)} | {SAMPLE COUNT} | {LABEL COUNT PER SAMPLE} | {ANNOTATION COUNT ACROSS ALL SAMPLES} | {DESCRIPTION} |


## How to Download the Source Dataset
### Version: original_update00
To download version "original_update00" ...... {HOW TO DOWNLOAD}  


## Example Samples and Annotations
### Version: original_update00
#### Tag: default
###### Sample Image                     
![alt text][sample_image]

[sample_image]: example_samples/sample_image.png
###### Sample annotation
![alt text][sample_annotation]

[sample_annotation]: example_samples/sample_annotation.png
              

## Installation
### Prerequisites
Ensure the system satisfies the following requirements: 
* git
* git lfs (Optional, if large files like EDA report are need to be downloaded with cloning of this repository)
* python >= 3.9 or docker >= 19.03

### Clone Git Repository (Required)
Clone the [git repository](/) into your <working_dir> and update the submodules.  
Please make sure to use the **HTTPS based clone link** as all the submodules are added using the HTTPS protocol.  
```
git clone <clone_link> <working_dir>
cd <working_dir>
source fetch_submodules.sh
```

### Setup Virtual Environment and Install Dependencies (Optional, recommended for debugging purpose only)
If datatool is intended to be used with python, it is strongly advised that the user creates [Python virtual environment](https://docs.python.org/3/library/venv.html) and install dependencies all the dependency python modules within the environment. This prevents risk of breaking existing system-wide packages. To create a virtual environment using Python's builtin `venv` module and activate:
```
python3 -m venv </path/to/new/virtual/environment>
source </path/to/new/virtual/environment>/bin/activate
```
To install the dependency modules that are required by the datatool inside virtual environment:
```
pip3 install -r datatool_api/deps/requirements.txt -r common/deps/requirements.txt -r deps/requirements.txt
``` 


## Dataset Access
In order to create the standardized output by the datatool, the datatool first needs to be able to access and read the source dataset. There are multiple ways in which the datatool can access and read the source dataset:  

### Local File System based Access (Manual Download)
The user can manually download the dataset version that needs to be processed by the datatool, and place the unzipped dataset contents in a directory on the local file system. For download instructions, please refer to [How to Download the Source Dataset](#how-to-download-the-source-dataset).  
  
To ensure that the datatool correctly processes the dataset version and all the associated tags, the user needs to follow certain naming conventions while placing the downloaded dataset on the local file system. The contents of the downloaded and unzipped dataset must be placed in a structure like:
```
<Root Directory>
 │   
 └─── <DATATOOL_NAME>
       │   
       └─── <DATATOOL_VERSION>
             │   
             └─── <DATATOOL_TAG>
                   │   
                   └─── <Downloaded dataset unzipped contents for the tag>
```
Please refer to the `datatool_name` field from [version_config](/config_files/version_config.yml) for correct resolution for DATATOOL_NAME. Below is an example of how to place the downloaded dataset for version **original_update00** and physical tag **default**:
![alt text][how_to_place_dataset_locally]

[how_to_place_dataset_locally]: example_samples/how_to_place_dataset_locally.png

### Remote Synology Web API based Access (Automatic Download)
If the datatool creator has pre-downloaded and placed the dataset in a synology NAS server, the datatool can automatically download the dataset from the synology NAS server using the [synology REST API](https://global.download.synology.com/download/Document/Software/DeveloperGuide/Os/DSM/All/enu/DSM_Login_Web_API_Guide_enu.pdf).  
In order for the datatool to be able to download the dataset from synology NAS serer, the user needs to provide the authentication credentials before running the datatool.
```
export DATATOOL_USER="<User Name for synology NAS server>"
export DATATOOL_PASSWORD="<User Password for synology NAS server>"
```

### Remote Amazon S3 based Access (Automatic Download)
If the datatool creator has pre-downloaded and placed the dataset in an amazon S3 bucket, the datatool can automatically download the dataset from the S3 bucket using the amazon [S3 API](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html).  
In order for the datatool to be able to download the dataset from s3, the user needs to provide the authentication credentials before running the datatool.
```  
export AWS_ACCESS_KEY="<aws_access_key>"
export AWS_ACCESS_KEY="<aws_secret_key>"
```
The user needs to ensure that at least one of the three aforementioned access methods are available for usage by the datatool before running the datatool.


## Usage
### Run Datatool
#### Python based (Recommended only for debugging purpose)
Please ensure that all python dependency modules have been installed by following [Setup Virtual Environment and Install Dependencies](#setup-virtual-environment-and-install-dependencies-optional-recommended-for-debugging-purpose-only).  
To process the dataset and generate the standardized output using the datatool main interface run the following command:
```
python3 datatool.py \
    --output-dir <OUTPUT_DIRECTORY> \
    --version <VERSION> \
    --tags <LIST_OF_TAGS> \
    --opeation-mode <OPERATION_MODE> \
    --validate-sample-paths <VALIDATE_SAMPLE_PATHS> \
    --storage <STORAGE> \
    --storage-loc <STORAGE_LOC> \
    --storage-root <STORAGE_ROOT>
```
Required arguments:
```
    --output-dir, -o        Directory to download and unpack the dataset
```

Optional version arguments:
```
    --version, -v               Version of the datatool to run, this overrides the current version set in the datatool config
    --tags, -t                  List of tags to be processed (if not specified, all available tags for the datatool version are processed)
```

Optional operational arguments:
```
    --operation-mode, -om         Operation mode for the datatool, this overrides the default mode set in datatool config
    --validate-sample-paths, -vl  If sample paths must be validated while dumping the dataset json to disk, this overrides the default value set in datatool config
```

Advance optional arguments:
```
    --storage, -s               Type of storage, [synology, s3 or local], this value overrides the default storage set in datatool config
    --storage-loc, -l           Address for remote storage (only used if storage is of type synology), this value overrides the location set in datatool config
    --storage-root, -r          Root directory holding the dataset:
                                For local storage: absolute path to the dataset root directory,
                                For synology storage: path to the synology root directory,
                                For s3 storage: bucket name of the bucket holding the dataset
```
This will create output directory (if it doesn't exist) and put dataset samples (Images, Video or Audio files) and `dataset.json` in it for each processed tag.

#### Docker based
All the documentation on datatool Docker usage can be found at [Datatool Docker Page](/docker).

### Output
The file structure for the generated output is:
```
<Output directory>
│   
└─── <Tag Name>
│   
└───────── dataset.json (JSON file containing annotations for all samples)
│   
└───────── dataset_sample.json (JSON file containing annotations for one sample only)
│   
└───────── sample_files (All dataset images / Videos /Audios)
    ├── 00001.xxx
    ├── 00002.xxx
    ├── 00003.xxx
    │   ...
    │   ...
    ├── 35887.xxx
```

### Exploratory Data Analysis (EDA) Report
The Exploratory data analysis report is present in [EDA Report](/eda_report) which provides the statistical insights on the annotations in the dataset.   
The report is generated by [EDA Reporting Tool](https://gitlab.com/bonseyes/artifacts/data_tools/eda-tool).  

#### To Regenerate EDA Report:
```
source create_EDA_report.sh \
    --input-dir <INPUT_DIR> \
    --output-dir <OUTPUT_DIR> \
    --report-type <REPORT_TYPE> \
    --operation-mode <OPERATION_MODE>
```

Required arguments:
```
    --input-dir, -i        Directory where the datatool output is stored (where dataset.json can be found)
    --output-dir, -o       Output directory where to store the EDA report
```
Optional arguments:
```
    --report-type, -r       Type of report to generate, currently supported types are
                            1: Enhanced Dataprep EDA Report
    --operation-mode, -om,  Datatool operation mode [memory, disk, ramdisk]
```

### Plot Annotations
Once the datatool has run and created the standardized output, it is possible to randomly draw some samples and plot the annotations for qualitative verification. The documentation on how to plot annotations is available in the [Plot Annotations page](/plot_annotations).Below is an overview collage generated by the plotting tool.  
![overview_collage][overview_collage]

[overview_collage]: example_samples/overview_collage.png

### Data Loader Examples
The standardized datatool output can be easily loaded and accessed via the [Datatool API](https://gitlab.com/bonseyes/artifacts/data_tools/apis/datatool-api) for subsequent tasks such as training, benchmarking etc. The [Data Loader Examples page](/example_dataloaders) provides example of how to use the datatool API to load and easily access the data samples and annotations using python based interface.

## Maintenance
Current maintainer ({Name}, {Email})
