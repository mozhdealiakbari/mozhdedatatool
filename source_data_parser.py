import os
import json
from common.data_processing.source_data_reader import SourceDataReader
from common.data_processing.source_data_parser_interface import SourceDataParserInterface
from common.data_processing.data_parsing_utils import DataParsingUtils
from datatool_api.models.BaseTypes import *
from datatool_api.models.Subject import Subject
from datatool_api.models.Object import Object
from datatool_api.models.ImageSample import ImageSample
from datatool_api.models.VideoInterval import VideoInterval
from datatool_api.models.VideoSample import VideoSample
from datatool_api.models.AudioSample import AudioSample
from datatool_api.models.Categories import Categories
from datatool_api.models.GlobalAnnotations import GlobalAnnotations
from datatool_api.models.DTDataset import DTDataset
import shutil
from tqdm import tqdm
from custom_base_types import *
from custom_dataset_model import DTDatasetCustom


class SourceDataParser(SourceDataParserInterface):
    def __init__(self):
        self.samples_dir = 'sample_files'

        # Add any needed local variables here

    """
    Implement parse_file() method if you are working with NVISO provided data model and base types.
    Uncomment and finish the example code and remove "NotImplementedError" statement from the end in order to provide 
    the implementation.
    """
    def parse_file(self, reader: SourceDataReader, file_name: str, file_ext: str, dataset: DTDataset,
                   datatool_version: str, datatool_tag: str):
        """
        Parse the source dataset file by file, where files are received in this method in a random order.
        The goal of this methods is to read the file if needed using "reader" and extract the annotations and fill the
        provided "dataset" instance with the annotations. If it's not possible to create the annotations in this method,
        store the extracted annotations in some auxiliary storage and use the "post_process" method to finally add
        annotations to the dataset.

        :param reader: The reader instance which will return the file contents if the file needs to be read
        :param file_name: Name of the file
        :param file_ext: Extension of the file
        :param dataset: NVISO dataset model instance
        :param datatool_version: datatool version which is under processing
        :param datatool_tag: datatool tag which is under processing
        :return: None
        """

        """
        # Example
        if datatool_version == 'original_update00':
            if datatool_tag == 'default':
                # Save the images to output directory if .png, .jpg, etc,
                # Example:
                if file_ext in ['.png', '.jpg', '.jpeg']:
                    DataParsingUtils.save_sample(file_name, file_ext, self.samples_dir,
                                                 reader.read_file(file_name, file_ext))
                elif file_ext == '.json':
                    data = json.loads(reader.read_file(file_name, file_ext).decode('utf-8'))

                    # Create an image sample
                    sample = ImageSample(id=os.path.join(self.samples_dir, file_name + file_ext),
                                         samplePath=os.path.join(self.samples_dir, file_name + file_ext))

                    # Add metadata to sample
                    sample_metadata = ImageSampleMetadata(colorSpace=Categories.ColorSpace.grayscale)
                    sample.metadata = sample_metadata

                    # Create subject and add annotations to it
                    subject = Subject()
                    subject.add_annotation(...)

                    # Add subject to sample
                    sample.subjects.add(subject)

                    # Add sample to the dataset
                    dataset.imageSamples.add(sample.id, sample)
                pass

            elif datatool_tag == ...:
                pass

        elif datatool_version == ...:
            pass
        """
        raise NotImplementedError

    """
    Implement post_process() method if you are working with NVISO provided data model and annotations could not be 
    created in parse_file method(). 
    Use this method to consolidate the extracted annotations from auxiliary storage and add them to NVISO dataset model 
    instance "dataset" provided in the method arguments.
    """
    def post_process(self, dataset: DTDataset, datatool_version: str, datatool_tag: str):
        """
        Any post processing that needs to happen after parse_file() has finished parsing through all source dataset
        files

        :param dataset: NVISO dataset model instance
        :param datatool_version: datatool version which is under processing
        :param datatool_tag: datatool tag which is under processing
        :return: None
        """
        # ADD any post-processing code here which you need to run after going through all the files
        # Make cases for different tags if required like "parse_file()"
        pass

    """
    Implement extract() method if you are working with custom data model and base models.
    Uncomment and finish the example code and remove "NotImplementedError" statement from the end in order to provide 
    the implementation
    """
    def extract(self, dataset_path: str, dataset: DTDatasetCustom, datatool_version: str, datatool_tag: str,
                output_dir: str):
        """
        Extract the annotations and add them to the custom data model instance

        :param dataset_path: path to the source dataset
        :param dataset: custom data model instance
        :param datatool_version: datatool version under processing
        :param datatool_tag: datatool tag under processing
        :param output_dir: Output directory
        :return: None
        """
        raise NotImplementedError

