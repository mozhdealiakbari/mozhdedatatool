"""
This Module defines the basic models for all annotations available in the dataset and
creates their data model through python classes.

For more details on how to easily create your data models please refer to the documentation at:
            [https://gitlab.com/bonseyes/artifacts/data_tools/data-validation-api]
"""

from __future__ import annotations
from datatool_api.data_validation.base_model import BaseModel
from datatool_api.data_validation.attribute import Attribute
from typing import List, Dict, Union
from enum import Enum
from datatool_api.data_validation.containers.ValList import ValList

# TODO - Add your custom base types here
